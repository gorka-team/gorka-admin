import axios from "axios";

axios.defaults.baseURL = 'https://iu-s21-sqr-gorka-backend.herokuapp.com';

export const getSportsmen = () => axios.get('/sportsman').then((res) => res.data && res.data.content);
export const mapSportsmenForSelect = ({id, alias}) => ({name: alias, id})

export const getSportsman = id => axios.get(`/sportsman/${id}`,).then((res) => res.data);
export const getSporsmanAlias = id => getSportsman(id).then(({alias}) => alias);
