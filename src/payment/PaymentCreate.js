import {BooleanInput, Create, DateTimeInput, SelectInput, SimpleForm, TextInput} from 'react-admin';
import React, {useEffect, useState} from "react";
import {getSportsmen, mapSportsmenForSelect} from "../dataprovider/requests";

export const PaymentCreate = props => {
    const [sportsmen, setSportsmen] = useState([]);

    useEffect(() => {
        return getSportsmen()
            .then(items => setSportsmen(items.map(mapSportsmenForSelect)))
    }, []);

    return (
        <Create {...props}>
            <SimpleForm>
                <SelectInput label="Sportsman" source="sportsmanId" choices={sportsmen}/>
                <TextInput source="amount"/>
                <DateTimeInput source="paymentDate"/>
                <BooleanInput source="isPaid" defaultValue={false}/>
            </SimpleForm>
        </Create>
    )
};