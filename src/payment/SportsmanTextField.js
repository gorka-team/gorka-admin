import React, {useEffect, useState} from "react";
import {TextField} from 'react-admin';
import {getSporsmanAlias} from "../dataprovider/requests";


export const SportsmanTextField = props => {
    const [alias, setAlias] = useState(props.record.sportsmanId);

    useEffect(
        () => getSporsmanAlias(props.record.sportsmanId)
            .then(newAlias => setAlias(newAlias))
        , []
    )

    return (
        <TextField label="Author Name" record={{alias}} source="alias"  />
    )
}