import {BooleanField, Datagrid, DateField, EditButton, List, TextField} from 'react-admin';
import {SportsmanTextField} from "./SportsmanTextField";
import React from "react";

export const PaymentList = props => (
    <List {...props}>
        <Datagrid>
            <TextField source="id"/>
            <SportsmanTextField source="sportsman"/>
            <TextField source="amount"/>
            <DateField source="paymentDate"/>
            <BooleanField source="isPaid"/>
            <EditButton/>
        </Datagrid>
    </List>
);