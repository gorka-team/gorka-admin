import {BooleanInput, Edit, DateTimeInput, SimpleForm, TextInput, SelectInput} from 'react-admin';
import {getSportsmen, mapSportsmenForSelect} from "../dataprovider/requests";
import {useEffect, useState} from "react";

export const PaymentEdit = props => {
    const [sportsmen, setSportsmen] = useState([]);

    useEffect(() => {
        return getSportsmen()
            .then(items => setSportsmen(items.map(mapSportsmenForSelect)))
    }, []);

    return (
        <Edit {...props}>
            <SimpleForm>
                <SelectInput label="Sportsman" source="sportsmanId" choices={sportsmen}/>
                <TextInput source="amount"/>
                <DateTimeInput source="paymentDate"/>
                <BooleanInput source="isPaid"/>
            </SimpleForm>
        </Edit>
    )
};