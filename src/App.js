import * as React from "react";
import {Admin, Resource} from 'react-admin';
import DataProvider from "./dataprovider/DataProvider";
import {SportsmanList} from "./sportsman/SportsmanList";
import {SportsmanEdit} from "./sportsman/SportsmanEdit";
import {SportsmanCreate} from "./sportsman/SportsmanCreate";
import {TrainingList} from "./training/TrainingList";
import {TrainingEdit} from "./training/TrainingEdit";
import {TrainingCreate} from "./training/TrainingCreate";
import {NewsList} from "./news/NewsList";
import {NewsEdit} from "./news/NewsEdit";
import {NewsCreate} from "./news/NewsCreate";
import {PaymentList} from "./payment/PaymentList";
import {PaymentEdit} from "./payment/PaymentEdit";
import {PaymentCreate} from "./payment/PaymentCreate";

const App = () => <Admin dataProvider={DataProvider('https://iu-s21-sqr-gorka-backend.herokuapp.com')}>
    <Resource name="sportsman" list={SportsmanList} edit={SportsmanEdit} create={SportsmanCreate}/>
    <Resource name="training" list={TrainingList} edit={TrainingEdit} create={TrainingCreate}/>
    <Resource name="news" list={NewsList} edit={NewsEdit} create={NewsCreate}/>
    <Resource name="payment" list={PaymentList} edit={PaymentEdit} create={PaymentCreate}/>
</Admin>;

export default App;