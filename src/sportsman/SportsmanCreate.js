import {Create, SimpleForm, TextInput} from 'react-admin';

export const SportsmanCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="fullName"/>
            <TextInput source="alias"/>
            <TextInput source="notes"/>
            <TextInput source="phone"/>
        </SimpleForm>
    </Create>
);