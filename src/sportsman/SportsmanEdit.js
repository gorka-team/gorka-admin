import {Edit, SimpleForm, TextInput} from 'react-admin';

export const SportsmanEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="fullName"/>
            <TextInput source="alias"/>
            <TextInput source="notes"/>
            <TextInput source="phone"/>
        </SimpleForm>
    </Edit>
);