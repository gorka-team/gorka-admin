import {Datagrid, EditButton, List, TextField} from 'react-admin';
import SimpleArray from "../common/SimpleArray";

export const SportsmanList = props => (
    <List {...props}>
        <Datagrid>
            <TextField source="id"/>
            <TextField source="fullName"/>
            <TextField source="alias"/>
            <TextField source="notes"/>
            <TextField source="phone"/>
            <SimpleArray source="trainings" label="Trainings"/>
            <EditButton/>
        </Datagrid>
    </List>
);