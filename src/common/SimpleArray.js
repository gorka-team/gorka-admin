import * as PropTypes from "prop-types";
import {Chip} from "@material-ui/core";

function populateList(elements) {
    return elements.map((element) => <Chip label={element}/>);
}

const SimpleArray = ({source, record}) =>
    <>
        {
            populateList(record[source])
        }
    </>;


SimpleArray.defaultProps = {
    addLabel: true,
    label: 'List'
};


SimpleArray.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string
};

export default SimpleArray;