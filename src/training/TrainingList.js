import {Datagrid, DateField, EditButton, List, TextField} from 'react-admin';
import SimpleArray from "../common/SimpleArray";

export const TrainingList = props => (
    <List {...props}>
        <Datagrid>
            <TextField source="id"/>
            <SimpleArray source="sportsmen" label="Sportsmen"/>
            <DateField source="startTime" showTime={true}/>
            <TextField source="notes"/>
            <EditButton/>
        </Datagrid>
    </List>
);