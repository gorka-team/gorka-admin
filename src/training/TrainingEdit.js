import {DateTimeInput, Edit, SimpleForm, TextInput, AutocompleteArrayInput} from 'react-admin';
import {useEffect, useState} from "react";
import {getSportsmen, mapSportsmenForSelect} from "../dataprovider/requests";

export const TrainingEdit = props => {
    const [sportsmen, setSportsmen] = useState([]);

    useEffect(() => {
        return getSportsmen()
            .then(items => setSportsmen(items.map(mapSportsmenForSelect)))
    }, []);

    return (
        <Edit {...props}>
            <SimpleForm>
                <AutocompleteArrayInput source="sportsmen" choices={sportsmen} />
                <DateTimeInput source="startTime"/>
                <TextInput source="notes"/>
            </SimpleForm>
        </Edit>
    )
};