import {AutocompleteArrayInput, Create, DateTimeInput, SimpleForm, TextInput} from 'react-admin';
import {useEffect, useState} from "react";
import {getSportsmen, mapSportsmenForSelect} from "../dataprovider/requests";

export const TrainingCreate = props => {
    const [sportsmen, setSportsmen] = useState([]);

    useEffect(() => {
        return getSportsmen()
            .then(items => setSportsmen(items.map(mapSportsmenForSelect)))
    }, []);

    return (
        <Create {...props}>
            <SimpleForm>
                <AutocompleteArrayInput source="sportsmen" choices={sportsmen} />
                <DateTimeInput source="startTime"/>
                <TextInput source="notes"/>
            </SimpleForm>
        </Create>
    )
};