import {Create, SimpleForm, TextInput} from 'react-admin';
import {NewsBodyField} from "./NewsBodyField";

export const NewsCreate = props => {
    return (
        <Create {...props}>
            <SimpleForm>
                <TextInput source="title"/>
                <NewsBodyField source="body"/>
            </SimpleForm>
        </Create>
    )
};