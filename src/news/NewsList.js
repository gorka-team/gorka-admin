import {Datagrid, EditButton, List, TextField} from 'react-admin';

export const NewsList = props => (
    <List {...props}>
        <Datagrid>
            <TextField source="id"/>
            <TextField source="timestamp"/>
            <TextField source="title"/>
            <EditButton/>
        </Datagrid>
    </List>
);