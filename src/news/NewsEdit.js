import {Edit, SimpleForm, TextInput} from 'react-admin';
import {NewsBodyField} from "./NewsBodyField";

export const NewsEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="title"/>
            <NewsBodyField source="body"/>
        </SimpleForm>
    </Edit>
);