import { useInput } from 'react-admin';
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import {CKEditor} from "@ckeditor/ckeditor5-react";

export const NewsBodyField = props => {
    const {
        input: { onChange, value },
    } = useInput(props);

    return (
        <CKEditor
            editor={ ClassicEditor }
            data={value}
            onReady={ editor => {
                // You can store the "editor" and use when it is needed.
                console.log( 'Editor is ready to use!', editor );
            } }
            onChange={ ( event, editor ) => {
                const data = editor.getData();
                console.log( { event, editor, data } );

                onChange(data);
            } }
        />
    );
};